//dar funciones en la página
console.log('hello world');

(function () {
  const MAIN_OBJ = {
    init: function () {
      console.log('load object...');

      this.eventHandlers(); //activo las funciones
    },

    eventHandlers: function () {
      document.querySelector('.hamburger-icon').addEventListener('click', function(){ //que me active el hamburger cuando de click
        document.querySelector('.menu-container').classList.toggle('menu-open'); //activa el menu open
      });

        document.querySelector('.sub-hamburger-icon').addEventListener('click', function(){ 
          document.querySelector('.sub-menu-container').classList.toggle('sub-menu-open');
        });
    }
  }

  MAIN_OBJ.init();
})();